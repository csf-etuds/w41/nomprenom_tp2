# Netflix

Projet de travail pratique (1-2-3) pour le cours de Services Web du Cégep de Sainte-Foy.

## Installation

1. Forker le Projet dans votre groupe 'servicesweb'
2. Dans votre IDE préféré, importer un projet maven avec ces fichiers
3. Lancer l'application par le main.

## Usage

Utiliser Postman pour vérifier vos routes et obtenir et modifier les ressources du service web.

## Contribution
Aucune contribution n'est accepté

## License
[MIT](https://choosealicense.com/licenses/mit/)
