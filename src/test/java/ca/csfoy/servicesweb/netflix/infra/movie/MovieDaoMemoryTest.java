package ca.csfoy.servicesweb.netflix.infra.movie;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.csfoy.servicesweb.netflix.domaine.movie.Movie;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;

@Tag("Unitaire")
@ExtendWith(MockitoExtension.class)
class MovieDaoMemoryTest {

    private MovieDaoMemory dao = new MovieDaoMemory();

    @Test
    void whenIdExistsInDaoThenDoesExistIsTrue() {
        Movie movie = new Movie("Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        dao.insert(movie);

        Assertions.assertTrue(dao.doesExist(movie.getId()));
    }

    @Test
    void whenIdDoesNotExistInDaoThenDoesExistIsFalse() {
        Assertions.assertFalse(dao.doesExist("100"));
    }

    @Test
    void whenSelectingExistingIdThenCorrespondingMovieIsReturned() {
        Movie movie = new Movie("Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        dao.insert(movie);

        Movie existing = dao.selectById(movie.getId());

        Assertions.assertEquals(movie, existing);
    }

    @Test
    void whenSelectingMovieBySearchCriteriaNameThenMoviesAreReturned() {
        Movie movie1 = new Movie("Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        Movie movie2 = new Movie("The Great Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        Movie movie3 = new Movie("Lego Movie", "", LocalDate.of(2014, 2, 1), "Will Ferrel is the man upstairs!", MovieCategory.COMEDY, null);
        Movie movie4 = new Movie("Lego Movie 2", "", LocalDate.of(2019, 2, 2), "Will Ferrel is the man upstairs!", MovieCategory.COMEDY, null);
        dao.insert(movie1);
        dao.insert(movie2);
        dao.insert(movie3);
        dao.insert(movie4);

        List<Movie> movies = dao.selectByTitle("Titanic", null);

        Assertions.assertTrue(movies.contains(movie1));
        Assertions.assertFalse(movies.contains(movie2));
        Assertions.assertFalse(movies.contains(movie3));
        Assertions.assertFalse(movies.contains(movie4));
    }

    @Test
    void whenSelectingMovieBySearchCriteriaName2ThenMoviesAreReturned() {
        Movie movie1 = new Movie("Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        Movie movie2 = new Movie("The Great Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        Movie movie3 = new Movie("Lego", "", LocalDate.of(2014, 2, 1), "Will Ferrel is the man upstairs!", MovieCategory.COMEDY, null);
        Movie movie4 = new Movie("Lego", "", LocalDate.of(2019, 2, 2), "Will Ferrel is the man upstairs!", MovieCategory.COMEDY, null);
        dao.insert(movie1);
        dao.insert(movie2);
        dao.insert(movie3);
        dao.insert(movie4);

        List<Movie> movies = dao.selectByTitle("Lego", null);

        Assertions.assertTrue(movies.contains(movie3));
        Assertions.assertTrue(movies.contains(movie4));
        Assertions.assertFalse(movies.contains(movie1));
        Assertions.assertFalse(movies.contains(movie2));
    }

    @Test
    void whenSelectingMovieBySearchCriteriaYearThenMoviesAreReturned() {
        Movie movie1 = new Movie("Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        Movie movie2 = new Movie("The Great Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        Movie movie3 = new Movie("Lego Movie", "", LocalDate.of(2014, 2, 1), "Will Ferrel is the man upstairs!", MovieCategory.COMEDY, null);
        Movie movie4 = new Movie("Lego Movie 2", "", LocalDate.of(2019, 2, 2), "Will Ferrel is the man upstairs!", MovieCategory.COMEDY, null);
        dao.insert(movie1);
        dao.insert(movie2);
        dao.insert(movie3);
        dao.insert(movie4);

        List<Movie> movies = dao.selectByTitle(null, 1997);

        Assertions.assertTrue(movies.contains(movie1));
        Assertions.assertTrue(movies.contains(movie2));
        Assertions.assertFalse(movies.contains(movie3));
        Assertions.assertFalse(movies.contains(movie4));
    }

    @Test
    void whenSelectingMovieBySearchCriteriaTitleAndYearThenMoviesAreReturned() {
        Movie movie1 = new Movie("Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        Movie movie2 = new Movie("The Great Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        Movie movie3 = new Movie("Lego Movie", "", LocalDate.of(2014, 2, 1), "Will Ferrel is the man upstairs!", MovieCategory.COMEDY, null);
        Movie movie4 = new Movie("Lego Movie 2", "", LocalDate.of(2019, 2, 2), "Will Ferrel is the man upstairs!", MovieCategory.COMEDY, null);
        dao.insert(movie1);
        dao.insert(movie2);
        dao.insert(movie3);
        dao.insert(movie4);

        List<Movie> movies = dao.selectByTitle("Lego", 2014);

        Assertions.assertTrue(movies.contains(movie3));
        Assertions.assertFalse(movies.contains(movie1));
        Assertions.assertFalse(movies.contains(movie2));
        Assertions.assertFalse(movies.contains(movie4));
    }

    @Test
    void whenSelectingAllThenAllMoviesAreReturned() {
        Movie movie1 = new Movie("Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        Movie movie2 = new Movie("Lego Movie", "", LocalDate.of(2014, 2, 1), "Will Ferrel is the man upstairs!", MovieCategory.COMEDY, null);
        dao.insert(movie1);
        dao.insert(movie2);

        List<Movie> movies = dao.selectAll();

        Assertions.assertTrue(movies.size() >= 2);
        Assertions.assertTrue(dao.doesExist(movie1.getId()));
        Assertions.assertTrue(dao.doesExist(movie2.getId()));
    }

    @Test
    void whenUpdatingMovieThenMovieIsUpdated() {
        Movie movie1 = new Movie("Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        Movie movie2 = new Movie("Lego Movie", "", LocalDate.of(2014, 2, 1), "Will Ferrel is the man upstairs!", MovieCategory.COMEDY, null);
        dao.insert(movie1);

        dao.update(movie2.getId(), movie2);

        Assertions.assertEquals(movie2, dao.selectById(movie2.getId()));
    }

    @Test
    void whenDeletingMovieThenMovieIsDeleted() {
        Movie movie1 = new Movie("Titanic", "James Cameron", LocalDate.of(1997, 12, 14), "The ship sinks", MovieCategory.DRAMA, null);
        dao.insert(movie1);

        dao.delete(movie1.getId());

        Assertions.assertNull(dao.selectById(movie1.getId()));
    }
}
