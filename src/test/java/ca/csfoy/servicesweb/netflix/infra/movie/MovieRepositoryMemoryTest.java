package ca.csfoy.servicesweb.netflix.infra.movie;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.csfoy.servicesweb.netflix.controller.exception.DuplicateException;
import ca.csfoy.servicesweb.netflix.domaine.movie.Movie;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;

@Tag("Unitaire")
@ExtendWith(MockitoExtension.class)
class MovieRepositoryMemoryTest {

    @Mock
    private MovieDao movieDao;

    @InjectMocks
    private MovieRepositoryMemory repo;

    @Mock
    private Movie movie;

    @Test
    void whenCreatingMovieThenMovieInsertedInDao() {
        Mockito.when(movie.getTitle()).thenReturn("title");
        Mockito.when(movie.getPublishedDate()).thenReturn(LocalDate.now());
        Mockito.when(movieDao.selectByTitle("title", LocalDate.now().getYear())).thenReturn(List.of());
        
        repo.create(movie);

        Mockito.verify(movieDao).insert(movie);
    }

    @Test
    void whenCreatingDuplicateMovieThenErrorIsRaised() {
        Mockito.when(movie.getTitle()).thenReturn("title");
        Mockito.when(movie.getPublishedDate()).thenReturn(LocalDate.now());
        Mockito.when(movieDao.selectByTitle("title", LocalDate.now().getYear())).thenReturn(List.of());
        Mockito.when(movieDao.insert(movie)).thenThrow(DuplicateException.class);

        Assertions.assertThrows(DuplicateException.class, () -> repo.create(movie));
    }

    @Test
    void whenSavingMovieWithIdThenMovieInsertedInDao() {
        Mockito.when(movieDao.selectById("id")).thenReturn(movie);
        
        repo.save("id", movie);

        Mockito.verify(movieDao).update("id", movie);
    }

    @Test
    void whenGettingMovieByIdThenMovieRetrievedInDao() {
        Mockito.when(movieDao.selectById("id")).thenReturn(movie);

        Movie movieRetrived = repo.getBy("id");

        Assertions.assertSame(movieRetrived, movie);
        Mockito.verify(movieDao).selectById("id");
    }

    @Test
    void whenGettingMovieByTitleAndYearThenMovieRetrievedInDao() {
        Mockito.when(movieDao.selectByTitle("Titanic", 2009)).thenReturn(List.of(movie));

        List<Movie> movieRetrived = repo.getBy("Titanic", 2009);

        Assertions.assertEquals(movieRetrived, List.of(movie));
        Mockito.verify(movieDao).selectByTitle("Titanic", 2009);
    }

    @Test
    void whenGettingAllMoviesThenMoviesRetrievedInDao() {
        Mockito.when(movieDao.selectAll()).thenReturn(List.of(movie));

        List<Movie> movieRetrived = repo.getAll();

        Assertions.assertEquals(movieRetrived, List.of(movie));
        Mockito.verify(movieDao).selectAll();
    }

    @Test
    void whenGettingMovieByCategoryThenMovieRetrievedInDao() {
        Mockito.when(movieDao.selectByCategory(MovieCategory.ACTION)).thenReturn(List.of(movie));

        List<Movie> movieRetrived = repo.getBy(MovieCategory.ACTION);

        Assertions.assertEquals(movieRetrived, List.of(movie));
        Mockito.verify(movieDao).selectByCategory(MovieCategory.ACTION);
    }

    @Test
    void whenGettingNewReleasesThenNewReleasesRetrievedByDao() {
        Mockito.when(movieDao.selectNewRelease(MovieCategory.ACTION)).thenReturn(Set.of(movie));

        Set<Movie> movieRetrived = repo.getNewReleases(MovieCategory.ACTION);

        Assertions.assertEquals(movieRetrived, Set.of(movie));
        Mockito.verify(movieDao).selectNewRelease(MovieCategory.ACTION);
    }

    @Test
    void whenGettingBestMoviesThenBestMoviesRetrievedByDao() {
        Mockito.when(movieDao.selectBestMoviesByCategory(MovieCategory.ACTION)).thenReturn(Set.of(movie));

        Set<Movie> movieRetrived = repo.getBestMovies(MovieCategory.ACTION);

        Assertions.assertEquals(movieRetrived, Set.of(movie));
        Mockito.verify(movieDao).selectBestMoviesByCategory(MovieCategory.ACTION);
    }

    @Test
    void whenDeletingMovieThenMovieDeletedByDao() {
        repo.delete("id");

        Mockito.verify(movieDao).delete("id");
    }
}
