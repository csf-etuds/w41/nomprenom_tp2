package ca.csfoy.servicesweb.netflix.api;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import ca.csfoy.servicesweb.netflix.api.dto.FullActorDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullActorDtoWithoutId;
import ca.csfoy.servicesweb.netflix.domaine.ActorMother;

@Tag("Deployment")
@SpringBootTest
@AutoConfigureMockMvc
class ActorResourceTest {

    private static final Long ANY_ID = 1L;
    private static final String PATH_TO_TEST = ActorResource.RESOURCE_PATH;
    private static final String PATH_WITH_ID = "/" + ANY_ID;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getIfGetSuccessfulThenReturn200Ok() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(PATH_TO_TEST + PATH_WITH_ID).contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void getAllIfGetSuccessfulThenReturn200Ok() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(PATH_TO_TEST).contentType("application/json")).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
    }

    @Test
    void createIfCreateSuccessfulThenReturn201Created() throws Exception {
        FullActorDtoWithoutId dto1 = new FullActorDtoWithoutId(ActorMother.ANY_FIRSTNAME,
                ActorMother.ANY_LASTNAME,
                ActorMother.ANY_BIRTHDATE,
                ActorMother.ANY_BIRTHPLACE);

        mockMvc.perform(MockMvcRequestBuilders.post(PATH_TO_TEST).contentType("application/json").content(objectMapper.writeValueAsString(dto1)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }
    
    @Test
    void createWithTwoValidationsFailedThenReturn422UnprocessableEntity() throws Exception {
      //    TODO : À compléter pour le TP2-US3
    }

    @Test
    void updateIfUpdateSuccessfullThenReturn204NoContent() throws Exception {
        FullActorDto dto1 = new FullActorDto(ActorMother.ANY_ID,
                ActorMother.ANY_FIRSTNAME,
                ActorMother.ANY_LASTNAME,
                ActorMother.ANY_BIRTHDATE,
                ActorMother.ANY_BIRTHPLACE);

        mockMvc.perform(MockMvcRequestBuilders.put(PATH_TO_TEST + PATH_WITH_ID).contentType("application/json").content(objectMapper.writeValueAsString(dto1)))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andReturn();
    }
    
    @Test
    void updateWithIDThatDoesNotMatchFail422UnprocessableEntity() throws Exception {
        FullActorDto dto1 = new FullActorDto(ActorMother.ANY_ID,
                ActorMother.ANY_FIRSTNAME,
                ActorMother.ANY_LASTNAME,
                ActorMother.ANY_BIRTHDATE,
                ActorMother.ANY_BIRTHPLACE);

        mockMvc.perform(MockMvcRequestBuilders.put(PATH_TO_TEST + "/90").contentType("application/json").content(objectMapper.writeValueAsString(dto1)))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity())
                .andReturn();
    }

    @Test
    void deleteIfDeleteSuccessfulThenReturn204NoContent() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(PATH_TO_TEST + PATH_WITH_ID).contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andReturn();
    }
}
