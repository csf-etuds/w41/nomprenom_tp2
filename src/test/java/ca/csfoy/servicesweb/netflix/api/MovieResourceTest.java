package ca.csfoy.servicesweb.netflix.api;

import java.time.LocalDate;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDtoWithoutId;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;

@Tag("Deployment")
@SpringBootTest
@AutoConfigureMockMvc
class MovieResourceTest {

    private static final String ANY_ID = "1";
    private static final String ANY_OTHER_ID = "99";
    private static final String PATH_TO_TEST = MovieResource.RESOURCE_PATH;
    private static final String PATH_TO_SEARCH = MovieResource.PATH_SEARCH;
    private static final String PATH_WITH_ID = "/" + ANY_ID;
    private static final String PATH_WITH_OTHER_ID = "/" + ANY_OTHER_ID;
    private static final String ANY_TITLE = "Moxie";
    private static final LocalDate ANY_BIRTHDATE = LocalDate.now().minusYears(28);
    private static final String ANY_DIRECTOR = "Director!";
    private static final String ANY_DESCRIPTION = "Description!";

    @Autowired
    private ObjectMapper objectMapper;
    
    @Autowired
    private MockMvc mockMvc;

    @Test
    void getByIdIfGetSuccessfulThenReturn200Ok() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(PATH_TO_TEST + PATH_WITH_ID)
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }

    @Test
    void getByIdIfMovieDoesNotExistThenReturn404NotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(PATH_TO_TEST + PATH_WITH_OTHER_ID)
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andReturn();
    }

    @Test
    void getBySearchIfGetSuccessfulThenReturn200Ok() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(PATH_TO_TEST + PATH_TO_SEARCH)
                .queryParam("title", ANY_TITLE, "year", "2000")
                .contentType("application/json")).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
    }

    @Test
    void getAllIfGetSuccessfulThenReturn200Ok() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(PATH_TO_TEST).contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }

    @Test
    void createIfCreateSuccessfulThenReturn201Created() throws Exception {
        FullMovieDtoWithoutId dto = new FullMovieDtoWithoutId(ANY_TITLE, ANY_DIRECTOR, ANY_BIRTHDATE, ANY_DESCRIPTION, MovieCategory.COMEDY, Map.of());
        
        mockMvc.perform(MockMvcRequestBuilders.post(PATH_TO_TEST)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();
    }

    @Test
    void createWithTwoValidationsFailedThenReturn422UnprocessableEntity() throws Exception {
            //TODO : À compléter pour le TP2-US3
    }
    
    @Test
    void updateIfUpdateSuccessfulThenReturn204NoContent() throws Exception {
        FullMovieDto dto = new FullMovieDto(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_BIRTHDATE, ANY_DESCRIPTION, MovieCategory.COMEDY, Map.of());
        
        mockMvc.perform(MockMvcRequestBuilders.put(PATH_TO_TEST + PATH_WITH_ID)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void updateIfMovieNotFoundThenReturn404NotFound() throws Exception {
        FullMovieDto dto = new FullMovieDto(ANY_OTHER_ID, ANY_TITLE, ANY_DIRECTOR, ANY_BIRTHDATE, ANY_DESCRIPTION, MovieCategory.COMEDY, Map.of());
        
        mockMvc.perform(MockMvcRequestBuilders.put(PATH_TO_TEST + PATH_WITH_OTHER_ID)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(dto)))
        .andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();

    }

    @Test
    void updateWithIDThatDoesNotMatchFail422UnprocessableEntity() throws Exception {
        FullMovieDto dto = new FullMovieDto(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_BIRTHDATE, ANY_DESCRIPTION, MovieCategory.COMEDY, Map.of());

        mockMvc.perform(MockMvcRequestBuilders.put(PATH_TO_TEST + "/90").contentType("application/json").content(objectMapper.writeValueAsString(dto)))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity())
                .andReturn();
    }
    
    @Test
    void deleteIfDeleteSuccessfulThenReturn204NoContent() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(PATH_TO_TEST + PATH_WITH_ID).contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andReturn();
    }

    @Test
    void deleteIfMovieNotFoundThenReturn204NoContent() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(PATH_TO_TEST + PATH_WITH_OTHER_ID)
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andReturn();
    }
}
