package ca.csfoy.servicesweb.netflix.controller;

import java.time.LocalDate;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.csfoy.servicesweb.netflix.api.dto.FullActorDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDto;
import ca.csfoy.servicesweb.netflix.api.dto.LightMovieDto;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;
import ca.csfoy.servicesweb.netflix.domaine.movie.Movie;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;

@Tag("Unitaire")
@ExtendWith(MockitoExtension.class)
class MovieConverterTest {

    private static final String ANY_ID = "1";
    private static final String ANY_TITLE = "Moxie";
    private static final String ANY_ROLE = "Black Widow";
    private static final LocalDate ANY_DATE = LocalDate.now().minusYears(1);
    private static final String ANY_ACTOR_ID = "1";
    private static final String ANY_FIRSTNAME = "Scarlett";
    private static final String ANY_LASTNAME = "Johansson";
    private static final LocalDate ANY_BIRTHDATE = LocalDate.now().minusYears(28);
    private static final String ANY_BIRTHPLACE = "St-Louis, MI, USA";
    private static final String ANY_DIRECTOR = "Director!";
    private static final String ANY_DESCRIPTION = "Description!";
    private static final MovieCategory ANY_CATEGORY_API = MovieCategory.COMEDY;

    @Mock
    private ActorConverter actorConverter;

    @InjectMocks
    private MovieConverter converter;

    @Test
    void toMovieIfInputDtoValidThenMovieConverted() {
        Actor actor = new Actor(ANY_ACTOR_ID, ANY_FIRSTNAME, ANY_LASTNAME, ANY_BIRTHDATE, ANY_BIRTHPLACE);
        Movie movie = new Movie(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_DATE, ANY_DESCRIPTION, ANY_CATEGORY_API, Map.of(ANY_ROLE, actor));

        LightMovieDto dto = converter.fromMovie(movie);

        Assertions.assertEquals(ANY_ID, dto.id);
        Assertions.assertEquals(ANY_TITLE, dto.title);
        Assertions.assertEquals(MovieCategory.COMEDY, dto.category);
    }

    @Test
    void toMovieIfInputDetailsDtoValidThenMovieConverted() {
        FullActorDto actorDto1 = new FullActorDto(ANY_ACTOR_ID, ANY_FIRSTNAME, ANY_LASTNAME, ANY_BIRTHDATE, ANY_BIRTHPLACE);
        FullMovieDto dto = new FullMovieDto(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_DATE, ANY_DESCRIPTION, ANY_CATEGORY_API, Map.of(ANY_ROLE, actorDto1));

        Movie movie = converter.toMovieFromFullDto(dto);

        Assertions.assertEquals(ANY_ID, movie.getId());
        Assertions.assertEquals(ANY_TITLE, movie.getTitle());
        Assertions.assertEquals(ANY_DIRECTOR, movie.getDirector());
        Assertions.assertEquals(ANY_DATE, movie.getPublishedDate());
        Assertions.assertEquals(ANY_DESCRIPTION, movie.getDescription());
        Assertions.assertEquals(MovieCategory.COMEDY, movie.getCategory());
        Mockito.verify(actorConverter).toActorMap(Map.of(ANY_ROLE, actorDto1));
    }
}
