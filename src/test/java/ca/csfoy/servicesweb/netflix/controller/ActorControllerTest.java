package ca.csfoy.servicesweb.netflix.controller;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.csfoy.servicesweb.netflix.api.dto.FullActorDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullActorDtoWithoutId;
import ca.csfoy.servicesweb.netflix.api.dto.LightActorDto;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;
import ca.csfoy.servicesweb.netflix.domaine.actor.ActorRepository;

@Tag("Unitaire")
@ExtendWith(MockitoExtension.class)
class ActorControllerTest {

    private static final String ANY_ID = "1";
    private static final String ANY_OTHER_ID = "2";
    private static final String ANY_BIRTHPLACE = "Lexington, MD, USA";

    @Mock
    private ActorConverter converter;

    @Mock
    private ActorRepository repository;

    @InjectMocks
    private ActorController controller;

    @Test
    void getAllThenAllActorsReturned() {
        LightActorDto dto1 = new LightActorDto(ANY_ID, "Scarlett", "Johansson");
        LightActorDto dto2 = new LightActorDto(ANY_OTHER_ID, "Nathalie", "Portman");
        List<LightActorDto> dtos = Arrays.asList(dto1, dto2);
        Actor actor1 = new Actor(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28), ANY_BIRTHPLACE);
        Actor actor2 = new Actor(ANY_OTHER_ID, "Nathalie", "Portman", LocalDate.now().minusYears(29), ANY_BIRTHPLACE);
        List<Actor> actors = Arrays.asList(actor1, actor2);
        Mockito.when(repository.getAll()).thenReturn(actors);
        Mockito.when(converter.fromActorListToLightDtoList(actors)).thenReturn(dtos);

        List<LightActorDto> actorsReturned = controller.getActors();

        Mockito.verify(repository).getAll();
        Mockito.verify(converter).fromActorListToLightDtoList(actors);
        Assertions.assertEquals(dtos, actorsReturned);
    }

    @Test
    void getByIfIdValidThenActorReturned() {
        FullActorDto dto1 = new FullActorDto(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28), ANY_BIRTHPLACE);
        Actor actor1 = new Actor(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28), ANY_BIRTHPLACE);
        Mockito.when(repository.getBy(ANY_ID)).thenReturn(actor1);
        Mockito.when(converter.fromActor(actor1)).thenReturn(dto1);

        FullActorDto actorReturned = controller.getActor(ANY_ID);

        Mockito.verify(repository).getBy(ANY_ID);
        Mockito.verify(converter).fromActor(actor1);
        Assertions.assertEquals(dto1, actorReturned);
    }

    @Test
    void createIfInputValidThenActorCreated() {
        FullActorDtoWithoutId dto = new FullActorDtoWithoutId("Scarlett", "Johansson", LocalDate.now().minusYears(28), ANY_BIRTHPLACE);
        Actor actor = new Actor(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28), ANY_BIRTHPLACE);
        Mockito.when(converter.toActorFromFullDtoWithoutId(dto)).thenReturn(actor);

        controller.createActor(dto);

        Mockito.verify(converter).toActorFromFullDtoWithoutId(dto);
        Mockito.verify(repository).create(actor);
    }

    @Test
    void updateIfInputValidThenActorUpdated() {
        FullActorDto dto = new FullActorDto(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28), ANY_BIRTHPLACE);
        Actor actor = new Actor(ANY_ID, "Scarlett", "Johansson", LocalDate.now().minusYears(28), ANY_BIRTHPLACE);
        Mockito.when(converter.toActor(dto)).thenReturn(actor);

        controller.updateActor(dto.id, dto);

        Mockito.verify(converter).toActor(dto);
        Mockito.verify(repository).save(ANY_ID, actor);
    }

    @Test
    void deleteIfInputValidThenActorDeleted() {
        controller.deleteActor(ANY_ID);

        Mockito.verify(repository).delete(ANY_ID);
    }
}
