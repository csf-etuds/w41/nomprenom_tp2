package ca.csfoy.servicesweb.netflix.controller;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import ca.csfoy.servicesweb.netflix.api.dto.FullActorDto;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;

@Tag("Unitaire")
class ActorConverterTest {

    private static final String ANY_ID = "1";
    private static final String ANY_FIRSTNAME = "Scarlett";
    private static final String ANY_LASTNAME = "Johansson";
    private static final LocalDate ANY_BIRTHDATE = LocalDate.now().minusYears(28);
    private static final String ANY_BIRTHPLACE = "Lexington, MD, USA";

    private ActorConverter converter = new ActorConverter();

    @Test
    void toActorIfInputDtoValidThenActorConverted() {
        FullActorDto dto1 = new FullActorDto(ANY_ID, ANY_FIRSTNAME, ANY_LASTNAME, ANY_BIRTHDATE, ANY_BIRTHPLACE);

        Actor actor = converter.toActor(dto1);

        Assertions.assertEquals(ANY_ID, actor.getId());
        Assertions.assertEquals(ANY_FIRSTNAME, actor.getFirstname());
        Assertions.assertEquals(ANY_LASTNAME, actor.getLastname());
        Assertions.assertEquals(ANY_BIRTHDATE, actor.getBirthdate());
        Assertions.assertEquals(ANY_BIRTHPLACE, actor.getBirthplace());
    }

    @Test
    void fromActorIfInputObjectValidThenActorConverted() {
        Actor actor1 = new Actor(ANY_ID, ANY_FIRSTNAME, ANY_LASTNAME, ANY_BIRTHDATE, ANY_BIRTHPLACE);

        FullActorDto dto = converter.fromActor(actor1);

        Assertions.assertEquals(ANY_ID, dto.id);
        Assertions.assertEquals(ANY_FIRSTNAME, dto.firstname);
        Assertions.assertEquals(ANY_LASTNAME, dto.lastname);
        Assertions.assertEquals(ANY_BIRTHDATE, dto.birthdate);
        Assertions.assertEquals(ANY_BIRTHPLACE, dto.birthplace);
    }
}
