package ca.csfoy.servicesweb.netflix.controller;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDtoWithoutId;
import ca.csfoy.servicesweb.netflix.api.dto.LightMovieDto;
import ca.csfoy.servicesweb.netflix.domaine.movie.Movie;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieRepository;

@Tag("Unitaire")
@ExtendWith(MockitoExtension.class)
class MovieControllerTest {

    private static final String ANY_ID = "1";
    private static final String ANY_TITLE = "Moxie";
    private static final String ANY_ACTOR = "Emma Stone";
    private static final Integer ANY_YEAR = 1998;
    private static final String ANY_DIRECTOR = "Director!";
    private static final String ANY_DESCRIPTION = "Description!";
    private static final LocalDate ANY_DATE = LocalDate.now().minusYears(1);
    private static final MovieCategory ANY_CATEGORY_API = MovieCategory.COMEDY;

    @Mock
    private MovieRepository repository;

    @Mock
    private MovieConverter converter;

    @InjectMocks
    private MovieController controller;

    @Test
    void getByIdIfInputValidThenMovieReturned() {
        Movie movie = new Movie(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_DATE, ANY_DESCRIPTION, ANY_CATEGORY_API, null);
        Mockito.when(repository.getBy(ANY_ID)).thenReturn(movie);
        FullMovieDto dto = new FullMovieDto(ANY_ID, ANY_TITLE, ANY_DIRECTOR, LocalDate.now().minusYears(1), ANY_DESCRIPTION, ANY_CATEGORY_API, null);
        Mockito.when(converter.toFullDtoFromMovie(movie)).thenReturn(dto);

        FullMovieDto movieReturned = controller.getById(ANY_ID);

        Mockito.verify(repository).getBy(ANY_ID);
        Mockito.verify(converter).toFullDtoFromMovie(movie);
        Assertions.assertEquals(dto, movieReturned);
    }

    @Test
    void getByTitleActorOrYearIfInputValidThenMovieReturned() {
        LightMovieDto dto1 = new LightMovieDto(ANY_ID, ANY_TITLE, ANY_CATEGORY_API);
        LightMovieDto dto2 = new LightMovieDto(ANY_ID, ANY_TITLE, ANY_CATEGORY_API);
        List<LightMovieDto> dtos = Arrays.asList(dto1, dto2);
        Movie movie1 = new Movie(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_DATE, ANY_DESCRIPTION, ANY_CATEGORY_API, null);
        Movie movie2 = new Movie(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_DATE, ANY_DESCRIPTION, ANY_CATEGORY_API, null);
        List<Movie> movies = Arrays.asList(movie1, movie2);
        Mockito.when(repository.getBy(ANY_TITLE, ANY_YEAR)).thenReturn(movies);
        Mockito.when(converter.fromMovie(movies)).thenReturn(dtos);

        List<LightMovieDto> moviesReturned = controller.search(ANY_TITLE, ANY_YEAR);

        Mockito.verify(repository).getBy(ANY_TITLE, ANY_YEAR);
        Mockito.verify(converter).fromMovie(movies);
        Assertions.assertEquals(dtos, moviesReturned);
    }

    @Test
    void getAllThenMoviesReturned() {
        LightMovieDto dto1 = new LightMovieDto(ANY_ID, ANY_TITLE, ANY_CATEGORY_API);
        LightMovieDto dto2 = new LightMovieDto(ANY_ID, ANY_TITLE, ANY_CATEGORY_API);
        List<LightMovieDto> dtos = Arrays.asList(dto1, dto2);
        Movie movie1 = new Movie(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_DATE, ANY_DESCRIPTION, ANY_CATEGORY_API, null);
        Movie movie2 = new Movie(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_DATE, ANY_DESCRIPTION, ANY_CATEGORY_API, null);
        List<Movie> movies = Arrays.asList(movie1, movie2);
        Mockito.when(repository.getAll()).thenReturn(movies);
        Mockito.when(converter.fromMovie(movies)).thenReturn(dtos);

        List<LightMovieDto> moviesReturned = controller.getAll();

        Mockito.verify(repository).getAll();
        Mockito.verify(converter).fromMovie(movies);
        Assertions.assertEquals(dtos, moviesReturned);
    }

    @Test
    void createIfInputValidThenMovieCreated() {
        FullMovieDtoWithoutId dto = new FullMovieDtoWithoutId(ANY_TITLE, ANY_DIRECTOR, LocalDate.now().minusYears(1), ANY_DESCRIPTION, ANY_CATEGORY_API, null);
        Movie movie = new Movie(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_DATE, ANY_DESCRIPTION, ANY_CATEGORY_API, null);
        Mockito.when(converter.toMovieFromFullDtoWithoutId(dto)).thenReturn(movie);

        controller.create(dto);

        Mockito.verify(converter).toMovieFromFullDtoWithoutId(dto);
        Mockito.verify(repository).create(movie);
    }

    @Test
    void updateIfInputValidThenMovieUpdated() {
        FullMovieDto dto = new FullMovieDto(ANY_ID, ANY_TITLE, ANY_DIRECTOR, LocalDate.now().minusYears(1), ANY_DESCRIPTION, ANY_CATEGORY_API, null);
        Movie movie = new Movie(ANY_ID, ANY_TITLE, ANY_DIRECTOR, ANY_DATE, ANY_DESCRIPTION, ANY_CATEGORY_API, null);
        Mockito.when(converter.toMovieFromFullDto(dto)).thenReturn(movie);

        controller.update(dto.id, dto);

        Mockito.verify(converter).toMovieFromFullDto(dto);
        Mockito.verify(repository).save(ANY_ID, movie);
    }

    @Test
    void deleteIfInputValidThenMovieDeleted() {
        controller.delete(ANY_ID);

        Mockito.verify(repository).delete(ANY_ID);
    }
}
