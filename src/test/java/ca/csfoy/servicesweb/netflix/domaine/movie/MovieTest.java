package ca.csfoy.servicesweb.netflix.domaine.movie;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ca.csfoy.servicesweb.netflix.domaine.MovieMother;

@Tag("Unitaire")
class MovieTest {

    private static Stream<Arguments> isMatchingCategoryArguments() {
        return Stream.of(Arguments.of(null, true),
                Arguments.of(MovieMother.ANY_CATEGORY, true),
                Arguments.of(MovieCategory.COMEDY, false),
                Arguments.of(MovieCategory.ANIMATION, false),
                Arguments.of(MovieCategory.DRAMA, false),
                Arguments.of(MovieCategory.FAMILY, false),
                Arguments.of(MovieCategory.HORROR, false),
                Arguments.of(MovieCategory.ROMANTIC_COMEDY, false),
                Arguments.of(MovieCategory.SUSPENSE, false));
    }

    @ParameterizedTest
    @MethodSource("isMatchingCategoryArguments")
    void doesMatchCategoryWithCategoryInputThenResultIsAsExpected(MovieCategory category, Boolean expectedResult) {
        Movie movie = MovieMother.getAnyMovieWithCasting();

        boolean result = movie.doesMatchCategory(category);

        Assertions.assertEquals(expectedResult, result);
    }

    private static Stream<Arguments> isMatchingSearchCriteriaArguments() {
        return Stream.of(Arguments.of(null, null, null, true),
                Arguments.of("", "", null, true),
                Arguments.of("Avengers", "Johansson", 2019, true),
                Arguments.of("", "Johansson", 2019, true),
                Arguments.of("Avengers", "", 2019, true),
                Arguments.of("Avengers", "", null, true),
                Arguments.of("", "Johansson", null, true),
                Arguments.of("", "", 2019, true),
                Arguments.of("Avengers", "Johansson", null, true),
                Arguments.of("Guardians", "Johansson", 2019, false),
                Arguments.of("Avengers", "Saldana", 2019, false),
                Arguments.of("Avengers", "Johansson", 2018, false),
                Arguments.of("Guardians", "", null, false),
                Arguments.of("", "Saldana", null, false),
                Arguments.of("", "", 2018, false));
    }

    @ParameterizedTest()
    @MethodSource("isMatchingSearchCriteriaArguments")
    void doesMatchSearchCriteriaWithInputCriteriaThenResultIsAsExpected(String title, String actor, Integer year, Boolean expectedResult) {
        Movie movie = MovieMother.getAnyMovieWithCasting();

        boolean result = movie.doesMatchSearchCriteria(title, actor, year);

        Assertions.assertEquals(expectedResult, result);
    }
}
