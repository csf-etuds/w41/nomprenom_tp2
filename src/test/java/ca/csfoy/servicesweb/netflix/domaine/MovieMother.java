package ca.csfoy.servicesweb.netflix.domaine;

import java.time.LocalDate;
import java.util.Map;

import ca.csfoy.servicesweb.netflix.domaine.movie.Movie;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;

public class MovieMother {

    public static final String ANY_ID = "1";
    public static final String ANY_TITLE = "Avengers Infinity War";
    public static final LocalDate ANY_RELEASE_DATE = LocalDate.of(2019, 5, 3);
    public static final LocalDate ANY_ACQUISITION_DATE = LocalDate.of(2021, 3, 21);
    public static final Integer ANY_RATING = 8;
    public static final String ANY_DIRECTOR = "Jon Favreau";
    public static final String ANY_DESCRIPTION = "Description!";
    public static final MovieCategory ANY_CATEGORY = MovieCategory.ACTION;
    public static final String ANY_ROLE = "Black Widow";
    public static final String ANY_OTHER_ROLE = "Jane Foster";

    public static Movie getAnyMovieWithCasting() {
        return new Movie(ANY_ID,
                ANY_TITLE,
                ANY_DIRECTOR,
                ANY_RELEASE_DATE,
                ANY_ACQUISITION_DATE,
                ANY_DESCRIPTION,
                ANY_CATEGORY,
                Map.of(ANY_ROLE, ActorMother.getAnyActor(), ANY_OTHER_ROLE, ActorMother.getAnyOtherActor()));
    }

    public static Movie getAnyMovieWithCasting(MovieCategory category) {
        return new Movie(ANY_ID,
                ANY_TITLE,
                ANY_DIRECTOR,
                ANY_RELEASE_DATE,
                ANY_ACQUISITION_DATE,
                ANY_DESCRIPTION,
                category,
                Map.of(ANY_ROLE, ActorMother.getAnyActor(), ANY_OTHER_ROLE, ActorMother.getAnyOtherActor()));
    }
}
