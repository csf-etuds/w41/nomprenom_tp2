package ca.csfoy.servicesweb.netflix.domaine;

import java.time.LocalDate;

import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;

public class ActorMother {

    public static final String ANY_ID = "1";
    public static final String ANY_FIRSTNAME = "Scarlett";
    public static final String ANY_LASTNAME = "Johansson";
    public static final LocalDate ANY_BIRTHDATE = LocalDate.of(1984, 11, 22);
    public static final String ANY_BIRTHPLACE = "Lexington, MD, USA";
    public static final String ANY_OTHER_ID = "2";
    public static final String ANY_OTHER_FIRSTNAME = "Nathalie";
    public static final String ANY_OTHER_LASTNAME = "Portman";
    public static final LocalDate ANY_OTHER_BIRTHDATE = LocalDate.of(1981, 6, 9);
    public static final String ANY_OTHER_BIRTHPLACE = "St-Louis, MI, USA";

    public static Actor getAnyActor() {
        return new Actor(ANY_ID, ANY_FIRSTNAME, ANY_LASTNAME, ANY_BIRTHDATE, ANY_BIRTHPLACE);
    }

    public static Actor getAnyActorWithFirstname(String firstname) {
        return new Actor(ANY_ID, firstname, ANY_LASTNAME, ANY_BIRTHDATE, ANY_BIRTHPLACE);
    }

    public static Actor getAnyActorWithLastname(String lastname) {
        return new Actor(ANY_ID, ANY_FIRSTNAME, lastname, ANY_BIRTHDATE, ANY_BIRTHPLACE);
    }

    public static Actor getAnyActorWithDate(LocalDate birthdate) {
        return new Actor(ANY_ID, ANY_FIRSTNAME, ANY_LASTNAME, birthdate, ANY_BIRTHPLACE);
    }

    public static Actor getAnyOtherActor() {
        return new Actor(ANY_OTHER_ID, ANY_OTHER_FIRSTNAME, ANY_OTHER_LASTNAME, ANY_OTHER_BIRTHDATE, ANY_OTHER_BIRTHPLACE);
    }

}
