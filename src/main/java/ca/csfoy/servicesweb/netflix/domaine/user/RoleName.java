package ca.csfoy.servicesweb.netflix.domaine.user;

//{TP2-US4}

public enum RoleName {
    ADMIN,
    USER;
    
    private String authority;
    
    RoleName() {
        this.authority = "ROLE_" + this.toString();
    }
    
    public String getAuthority() {
        return this.authority;
    }
}
