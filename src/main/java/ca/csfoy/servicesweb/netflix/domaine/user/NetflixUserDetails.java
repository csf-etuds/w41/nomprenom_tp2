package ca.csfoy.servicesweb.netflix.domaine.user;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

//{TP2-US4}

public class NetflixUserDetails implements UserDetails {

    private static final long serialVersionUID = 1L;
    private String emailAddress;
    private String password;
    private Role role;

    public NetflixUserDetails(String emailAddress, String password) {
        this.emailAddress = emailAddress;
        this.password = password;
        this.role = new Role("1", RoleName.USER);
    }
    
    public NetflixUserDetails(String emailAddress, String password, Role role) {
        this.emailAddress = emailAddress;
        this.password = password;
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return emailAddress;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(this.role.getAuthority()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
