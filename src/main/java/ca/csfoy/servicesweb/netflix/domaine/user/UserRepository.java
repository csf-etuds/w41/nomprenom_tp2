package ca.csfoy.servicesweb.netflix.domaine.user;

import ca.csfoy.servicesweb.netflix.domaine.UpdateRepository;

//{TP2-US4}

public interface UserRepository extends UpdateRepository<String, NetflixUser> {

    NetflixUser getByEmail(String email);
}
