package ca.csfoy.servicesweb.netflix.domaine;

public class GeneratorId {
    
    public static final int ID_INCREMENT = 1;

    private static int nextId = ID_INCREMENT;

    public static String getNextId() {
        int nextId = GeneratorId.nextId;
        updateIdSequence();
        return nextId + "";
    }

    private static void updateIdSequence() {
        GeneratorId.nextId = GeneratorId.nextId + ID_INCREMENT;
    }
    
    private GeneratorId() {}
}
