package ca.csfoy.servicesweb.netflix.domaine.actor;

import java.util.Collection;
import java.util.List;

import ca.csfoy.servicesweb.netflix.domaine.DeleteRepository;
import ca.csfoy.servicesweb.netflix.domaine.ReadRepository;
import ca.csfoy.servicesweb.netflix.domaine.UpdateRepository;

public interface ActorRepository extends ReadRepository<String, Actor>, UpdateRepository<String, Actor>, DeleteRepository<String, Actor> { 
    
    void saveAll(Collection<Actor> actor);
    
    List<Actor> getBy(String name, String lastname);
}