package ca.csfoy.servicesweb.netflix.domaine.user;

import java.util.Objects;

// {TP2-US4}

public class Role {

    private String identifier;
    private RoleName roleName;

    public Role(String identifier, RoleName roleName) {

        this.identifier = identifier;
        this.roleName = roleName;
    }

    public String getIdentifier() {
        return identifier;
    }
    
    public RoleName getRoleName() {
        return this.roleName;
    }

    public boolean isRoleAdmin() {
        return roleName == RoleName.ADMIN;
    }

    public boolean isRoleUser() {
        return roleName == RoleName.USER;
    }
    
    public String getAuthority() {
        return roleName.getAuthority();
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier, roleName);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }

        if (object instanceof Role) {
            Role that = (Role) object;

            return Objects.equals(this.identifier, that.identifier) && Objects.equals(this.roleName, that.roleName);
        }

        return false;
    }
}
