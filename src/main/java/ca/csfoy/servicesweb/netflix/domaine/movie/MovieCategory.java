package ca.csfoy.servicesweb.netflix.domaine.movie;

public enum MovieCategory {
    
    ACTION,
    ANIMATION,
    COMEDY,
    DRAMA,
    FAMILY,
    HORROR,
    ROMANTIC_COMEDY,
    SUSPENSE;
}
