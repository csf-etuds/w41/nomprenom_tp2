package ca.csfoy.servicesweb.netflix.domaine.movie;

import java.time.LocalDate;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import ca.csfoy.servicesweb.netflix.domaine.GeneratorId;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;

public class Movie {
    
    private String id;
    private String title;
    private String director;
    private LocalDate publishedDate;
    private LocalDate acquisitionDate;
    private String description;
    private MovieCategory category;
    private Map<String, Actor> casting;
        
    public Movie(String title, String director, LocalDate publishedDate, String description, MovieCategory category, Map<String, Actor> casting) {
        this(GeneratorId.getNextId(), title, director, publishedDate, LocalDate.now(), description, category, casting);
    }
    
    public Movie(String id, String title, String director, LocalDate publishedDate, String description, MovieCategory category, Map<String, Actor> casting) {
        this(id, title, director, publishedDate, LocalDate.now(), description, category, casting);
    }
    
    public Movie(String id, 
            String title, 
            String director, 
            LocalDate publishedDate, 
            LocalDate acquisitionDate,
            String description, 
            MovieCategory category,
            Map<String, Actor> casting) {
        this.id = id;
        this.title = title;
        this.director = director;
        this.publishedDate = publishedDate;
        this.acquisitionDate = acquisitionDate;
        this.description = description;
        this.category = category;
        this.casting = casting;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDirector() {
        return director;
    }

    public LocalDate getPublishedDate() {
        return publishedDate;
    }

    public LocalDate getAcquisitionDate() {
        return acquisitionDate;
    }

    public String getDescription() {
        return description;
    }

    public MovieCategory getCategory() {
        return category;
    }

    public Map<String, Actor> getCasting() {
        return casting;
    }

    public boolean doesMatchCategory(MovieCategory category) {
        if (Objects.nonNull(category)) {
            return this.category == category;
        }
        
        return true;
    }

    public boolean doesMatchSearchCriteria(String title, String actor, Integer year) {
        return isMatchingTitle(title) && isMatchingActor(actor) && isMatchingYear(year);
    }
    
    private boolean isMatchingTitle(String title) {
        if (!StringUtils.isBlank(title)) {
            return this.title.contains(title);
        }
        
        return true;
    }
    
    private boolean isMatchingActor(String actor) {
        if (!StringUtils.isBlank(actor)) {
            return casting.values().stream()
                    .anyMatch(actorInCasting -> actorInCasting.isMatching(actor));
        }
        
        return true;
    }
    
    private boolean isMatchingYear(Integer year) {
        if (Objects.nonNull(year)) {
            return year.equals(this.publishedDate.getYear());
        }
        
        return true;
    }   
    
    public boolean equals(Object movie) {
        if (this == movie) {
            return true;
        }
        
        if (movie instanceof Movie) {
            Movie that = (Movie) movie;
            
            return Objects.equals(this.id, that.id) &&
                    Objects.equals(this.title, that.title) &&
                    Objects.equals(this.director, that.director);
        }
        
        return false;
    }
    
    public int hashCode() {
        return Objects.hash(this.title);
    }
}
