package ca.csfoy.servicesweb.netflix.domaine.user;

import java.security.Principal;

//{TP2-US4}

public class UserPrincipal implements Principal {

    public final String id;
    public final String username;

    public UserPrincipal(String id, String username) {
        this.id = id;
        this.username = username;
    }

    @Override
    public String getName() {
        return username;
    }
}