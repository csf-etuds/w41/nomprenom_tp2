package ca.csfoy.servicesweb.netflix.domaine.movie;

import java.util.List;
import java.util.Set;

import ca.csfoy.servicesweb.netflix.domaine.DeleteRepository;
import ca.csfoy.servicesweb.netflix.domaine.ReadRepository;
import ca.csfoy.servicesweb.netflix.domaine.UpdateRepository;

public interface MovieRepository extends DeleteRepository<String, Movie>, UpdateRepository<String, Movie>, ReadRepository<String, Movie> {

    List<Movie> getBy(MovieCategory category);
    
    Set<Movie> getNewReleases(MovieCategory category);
    
    Set<Movie> getBestMovies(MovieCategory category);

    List<Movie> getBy(String title, Integer year);

}