package ca.csfoy.servicesweb.netflix.domaine;

/**
 * Dépôt de base avec une fonctionnalité ajoutée de modification.
 * 
 * @author cboileau
 *
 * @param <I> Le type de l'identifiant des objets stockés dans le dépôt
 * @param <E> Le type de l'objet stocké dans le dépôt
 */
public interface UpdateRepository<I, E> extends BasicRepository<I, E> {

    void save(I id, E element);
}
