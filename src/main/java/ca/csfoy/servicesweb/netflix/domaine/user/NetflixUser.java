package ca.csfoy.servicesweb.netflix.domaine.user;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class NetflixUser {

    private static final long serialVersionUID = 1L;
    
    //{TP2-US4}
    
    private String id;
    private String firstname;
    private String lastname;
    private NetflixUserDetails userDetails;

    public NetflixUser(String id,
            String firstname,
            String lastname,
            NetflixUserDetails userDetails) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.userDetails = userDetails;
    }

    public String getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmailAddress() {
        return this.userDetails.getUsername();
    }

    public String getPassword() {
        return this.userDetails.getPassword();
    }

    public Role getRole() {
        return this.userDetails.getRole();
    }
    
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.userDetails.getAuthorities();
    }
}
