package ca.csfoy.servicesweb.netflix.api;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ca.csfoy.servicesweb.netflix.api.dto.FullActorDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullActorDtoWithoutId;
import ca.csfoy.servicesweb.netflix.api.dto.LightActorDto;

@RequestMapping(value = ActorResource.RESOURCE_PATH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public interface ActorResource {


    String ID_PARAM = "id";
    String RESOURCE_PATH = "/actors";
    String PATH_WITH_ID = "/{" + ID_PARAM + "}";

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    FullActorDto createActor(@RequestBody FullActorDtoWithoutId author);

    @GetMapping(PATH_WITH_ID)
    FullActorDto getActor(@PathVariable(ID_PARAM) String id);

    @GetMapping()
    List<LightActorDto> getActors();

    @PutMapping(PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void updateActor(@PathVariable(ID_PARAM) String id, @RequestBody FullActorDto author);

    @DeleteMapping(PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteActor(@PathVariable(ID_PARAM) String id);
}