package ca.csfoy.servicesweb.netflix.api.dto;

import java.time.LocalDate;
import java.util.Map;

import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;

public class FullMovieDto {

    public final String id;
    public final String title;
    public final String director;
    public final LocalDate publishedDate;
    public final String description;
    public final MovieCategory category;
    public final Map<String, FullActorDto> casting;
    
    public FullMovieDto(String id, 
            String title, 
            String director, 
            LocalDate publishedDate, 
            String description, 
            MovieCategory category, 
            Map<String, FullActorDto> casting) {
        this.id = id;
        this.title = title;
        this.director = director;
        this.publishedDate = publishedDate;
        this.description = description;
        this.category = category;
        this.casting = casting;
    }
}
