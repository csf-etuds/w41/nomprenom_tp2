package ca.csfoy.servicesweb.netflix.api.dto;

public class UserDto {

    public final String id;
    public final String firstname;
    public final String lastname;

    public UserDto(String id, String firstname, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }
}