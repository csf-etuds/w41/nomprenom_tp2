package ca.csfoy.servicesweb.netflix.api.dto;

import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;

public class LightMovieDto {
    
    public final String id;
    public final String title;
    public final MovieCategory category;
    
    public LightMovieDto(String id, String title, MovieCategory category) {
        this.id = id;
        this.title = title;
        this.category = category;
    }
}
