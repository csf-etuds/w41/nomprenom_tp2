package ca.csfoy.servicesweb.netflix.api.dto;

public class FullUserDto {
    
    public final String id;
    public final String firstname;
    public final String lastname;
    public final String emailAddress;
    public final String password;

    public FullUserDto(String id,
            String firstname,
            String lastname,
            String emailAddress,
            String password) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.emailAddress = emailAddress;
        this.password = password;
    }
}
