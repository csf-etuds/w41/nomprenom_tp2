package ca.csfoy.servicesweb.netflix.api.dto;

import java.time.LocalDate;

public class FullActorDto {

    public final String id;
    public final String firstname;
    public final String lastname;
    public final LocalDate birthdate;
    
    public final String birthplace;

    public FullActorDto(String id, String firstname, String lastname, LocalDate birthdate, String birthplace) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthdate = birthdate;
        this.birthplace = birthplace;
    }
}
