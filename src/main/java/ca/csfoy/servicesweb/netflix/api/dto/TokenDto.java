package ca.csfoy.servicesweb.netflix.api.dto;

public class TokenDto {

    public final String token;

    public TokenDto(String token) {
        this.token = token;
    }
}
