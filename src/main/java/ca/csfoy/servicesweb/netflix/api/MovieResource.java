package ca.csfoy.servicesweb.netflix.api;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDtoWithoutId;
import ca.csfoy.servicesweb.netflix.api.dto.LightMovieDto;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

@RequestMapping(value = MovieResource.RESOURCE_PATH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public interface MovieResource {

    public static final String ID_PARAM = "id";
    public static final String TITLE_PARAM = "title";
    public static final String YEAR_PARAM = "year";
    public static final String RESOURCE_PATH = "/movies";
    public static final String PATH_WITH_ID = "/{" + ID_PARAM + "}";
    public static final String PATH_SEARCH = "/search";

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    FullMovieDto create(@RequestBody FullMovieDtoWithoutId movie);

    @GetMapping(PATH_WITH_ID)
    FullMovieDto getById(@PathVariable String id);

    @GetMapping
    List<LightMovieDto> getAll();

    @GetMapping(PATH_SEARCH)
    List<LightMovieDto> search(@RequestParam(value = TITLE_PARAM, required = false) String title, 
                               @RequestParam(value = YEAR_PARAM, required = false) Integer year);

    @PutMapping(PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void update(@PathVariable String id, @RequestBody FullMovieDto movie);

    @DeleteMapping(PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable String id);
}
