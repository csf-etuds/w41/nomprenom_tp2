package ca.csfoy.servicesweb.netflix.api;

public class UserValidationConstants {

    public static final String MSG_UUID_NOT_NULL_VALIDATION = "Identifier must not be nul, empty or blank.";
    public static final String MSG_UUID_FORMAT_VALIDATION = "Identifier must have a UUID format.";
    public static final String FORMAT_UUID = "^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$";
    public static final String MSG_STR_NOT_NULL_VALIDATION = "The value may not be null, empty or blank.";
    public static final String MSG_STR_MIN_MAX_LENGTH_VALIDATION = "The value must be between {min} and {max} characters.";
    public static final String MSG_EMAIL_NOT_NULL_VALIDATION = "Email must not be nul, empty or blank.";
    public static final String MSG_FORMAT_EMAIL_VALIDATION = "Email must have a valid email format.";
    public static final String MSG_PWD_NOT_NULL_VALIDATION = "Password must not be nul, empty or blank.";
    public static final String MSG_PWD_MIN_MAX_LENGTH_VALIDATION = "Password must have between {min} and {max} characters.";
    public static final String MSG_PASSWORD_STRENGTH_VALIDATION = "Password is not strong enough.";

}
