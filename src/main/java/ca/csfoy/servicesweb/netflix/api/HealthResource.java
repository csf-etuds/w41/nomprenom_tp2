package ca.csfoy.servicesweb.netflix.api;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Health Resource", description = "API for the Health Resource")
@RequestMapping(value = HealthResource.RESOURCE_PATH,
                produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_PROBLEM_JSON_VALUE},
                consumes=MediaType.APPLICATION_JSON_VALUE)
public interface HealthResource {

    public final static String RESOURCE_PATH = "/health";

    @Operation(summary = "Get a basic response from the server.")
    @ApiResponses(value = { 
            @ApiResponse(responseCode = "200", description = "The system is up and running.", 
                    content = { @Content(mediaType = "application/json") }) })
    @GetMapping
    public String getHealth();

}
