package ca.csfoy.servicesweb.netflix.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import ca.csfoy.servicesweb.netflix.api.dto.FullUserDto;
import ca.csfoy.servicesweb.netflix.api.dto.UserDto;

@RequestMapping(value = UserResource.RESOURCE_PATH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public interface UserResource {

    String RESOURCE_PATH = "/users";
    String PATH_PARAM_ID = "id";
    String PATH_WITH_ID = "/{" + PATH_PARAM_ID + "}";
    String PATH_SIGN_UP = "/signup";
    String PATH_RELEASES = "/releases";
    String PATH_SUGGESTIONS = "/suggestions";

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void createUser(@RequestBody UserDto user);

    @PostMapping(PATH_SIGN_UP)
    @ResponseStatus(HttpStatus.CREATED)
    void createUser(@RequestBody FullUserDto user);

    @GetMapping(PATH_WITH_ID)
    UserDto getUser(@PathVariable(PATH_PARAM_ID) String id);

    @PutMapping(PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void modifyUser(@PathVariable(PATH_PARAM_ID) String userId, @RequestBody UserDto user);
}
