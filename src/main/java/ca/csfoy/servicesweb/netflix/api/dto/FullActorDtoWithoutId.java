package ca.csfoy.servicesweb.netflix.api.dto;

import java.time.LocalDate;

public class FullActorDtoWithoutId {
    
    public final String firstname;
    public final String lastname;
    public final LocalDate birthdate;
    public final String birthplace;
    
    public FullActorDtoWithoutId(String firstname, String lastname, LocalDate birthdate, String birthplace) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthdate = birthdate;
        this.birthplace = birthplace;
    }
}
