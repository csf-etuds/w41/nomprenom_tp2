package ca.csfoy.servicesweb.netflix.api.dto;

public class LightActorDto {
    
    public final String id;
    public final String firstname;
    public final String lastname;
    
    public LightActorDto(String id, String firstname, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }
}
