package ca.csfoy.servicesweb.netflix.security;

import java.util.Base64;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import ca.csfoy.servicesweb.netflix.domaine.user.Role;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

// {TP2-US4}

@Component
public class JwtTokenManager {

    //TODO les propriétés suivantes doivent provenir du fichier de propriétés
    private String timeInMilliseconds = "300000";
    private String secretKey = "Htu.9pApYAoaHsc;c";
    
    public String createToken(String email, Role role) {
        Claims claims = Jwts.claims().setSubject(email);
        claims.put("auth", role);
        Date now = new Date();
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(new Date(now.getTime() + Long.parseLong(timeInMilliseconds)))
                .signWith(Keys.hmacShaKeyFor(encodeSecret()), SignatureAlgorithm.HS256)
                .compact();
    }
    
    public String getUserIdFromToken(String token) {
        return (String) this.getClaims(token).get("id");
    }
    
    public String getSubjectFromToken(String token) {
        return this.getClaims(token).getSubject();
    }

    public boolean validateToken(String token) {
        this.getClaims(token);
        return true;
    }
    
    private Claims getClaims(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(encodeSecret()))
                .build()
                .parseClaimsJws(token)
                .getBody();
    }
    
    private byte[] encodeSecret() {
        return Base64.getEncoder().encode(secretKey.getBytes());
    }
}
