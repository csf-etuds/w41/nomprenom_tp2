package ca.csfoy.servicesweb.netflix.security;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ca.csfoy.servicesweb.netflix.controller.LoggerUtils;
import ca.csfoy.servicesweb.netflix.controller.NetflixProblemDetailFactory;
import ca.csfoy.servicesweb.netflix.domaine.user.UserPrincipal;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

// {TP2-US4}

@Component
public class JwtTokenEntryPointFilter extends OncePerRequestFilter {

    private Logger logger = LoggerFactory.getLogger(JwtTokenEntryPointFilter.class);

    private final NetflixProblemDetailFactory problemDetailFactory;
    private final JwtTokenManager tokenManager;
    private final UserDetailsService userDetailsService;
    private final ObjectMapper objectMapper;

    public JwtTokenEntryPointFilter(NetflixProblemDetailFactory problemDetailFactory,
            JwtTokenManager tokenManager,
            UserDetailsService userDetailsService,
            ObjectMapper objectMapper) {
        this.problemDetailFactory = problemDetailFactory;
        this.tokenManager = tokenManager;
        this.userDetailsService = userDetailsService;
        this.objectMapper = objectMapper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader("Authorization");
        try {
            this.authenticate(token);
        } catch (RuntimeException authException) {
            this.setExceptionResponse(response, authException);
            return;
        }

        filterChain.doFilter(request, response);
    }

    private void authenticate(String token) {
        if (Objects.nonNull(token)) {
            String tokenWithoutBearer = token;
            if (token.startsWith("Bearer ")) {
                tokenWithoutBearer = token.substring(7);                
            }
            
            tokenManager.validateToken(tokenWithoutBearer);
            String email = tokenManager.getSubjectFromToken(tokenWithoutBearer);
            String id = tokenManager.getUserIdFromToken(tokenWithoutBearer);
            UserDetails user = userDetailsService.loadUserByUsername(email);

            SecurityContextHolder.getContext()
                    .setAuthentication(new UsernamePasswordAuthenticationToken(
                            new UserPrincipal(id, user.getUsername()),
                            user.getPassword(),
                            user.getAuthorities()));
        }
    }

    private void setExceptionResponse(HttpServletResponse response, Exception authException) {
        SecurityContextHolder.clearContext();
        String errorId = UUID.randomUUID().toString();
        //TODO : Créer un problemDetail en réutilisant le code en place pour la gestion des erreurs (factory)
        ProblemDetail problemDetail = null; // Corriger cette instruction
        LoggerUtils.logError(logger, authException, errorId);
        try {
            setResponse(response, problemDetail);
        } catch (IOException jpe) {
            throw new RuntimeException("An error occured in creating authentication exception response", jpe);
        }
    }

    private void setResponse(HttpServletResponse response, ProblemDetail problemDetail) throws JsonProcessingException, IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");
        response.getOutputStream().println(objectMapper.writeValueAsString(problemDetail));
    }
}
