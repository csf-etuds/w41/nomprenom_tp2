package ca.csfoy.servicesweb.netflix;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import ca.csfoy.servicesweb.netflix.security.JwtTokenEntryPointFilter;

@Configuration
/** {TP2-US4}: Pour l'authentification*/
@EnableWebSecurity 
/** {TP2-US4}: Pour l'autorisation sur les méthodes */
@EnableMethodSecurity
public class SecurityConfig {
    
    /** {TP2-US4}: Tous les @Bean nécessaires pour la sécurité (auth)*/
    @Bean
    public PasswordEncoder passwordEncoder() {
        //TODO : L'encodeur n'est pas adéquat, corriger selon les spécificatons de l'énoncé
        return new StandardPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authManager(HttpSecurity http, UserDetailsService userDetailsService) throws Exception {
        AuthenticationManagerBuilder authenticationManagerBuilder = http.getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
        return authenticationManagerBuilder.build();
    }
    
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, JwtTokenEntryPointFilter jwtFilter) throws Exception {
        return http.csrf(csrf -> csrf.disable())
                .headers(headers -> headers.disable())
                .sessionManagement(sessionManagement -> sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                // En ajoutant notre filtre (cette méthode) avant l'authentification par username/password,
                // on va avoir une authentification valide et les autres ne seront pas considérés.
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class)
                .authorizeHttpRequests(auth -> {
                    auth.anyRequest().permitAll(); //TODO à ajuster selon les spécifications de l'énoncé
                }).build();
    }
}