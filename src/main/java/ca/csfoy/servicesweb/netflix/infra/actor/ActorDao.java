package ca.csfoy.servicesweb.netflix.infra.actor;

import java.util.List;

import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;
import ca.csfoy.servicesweb.netflix.infra.Dao;

public interface ActorDao extends Dao<String, Actor> {

    List<Actor> findByFirstnameAndLastname(String firstname, String lastname);
} 
