package ca.csfoy.servicesweb.netflix.infra.movie;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.springframework.stereotype.Repository;

import ca.csfoy.servicesweb.netflix.controller.exception.DuplicateException;
import ca.csfoy.servicesweb.netflix.controller.exception.ObjectNotFoundException;
import ca.csfoy.servicesweb.netflix.domaine.movie.Movie;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieRepository;

@Repository
public class MovieRepositoryMemory implements MovieRepository {

    private final MovieDao dao;

    public MovieRepositoryMemory(MovieDao dao) {
        this.dao = dao;
    }

    @Override
    public Movie create(Movie movie) {
        List<Movie> movies = getBy(movie.getTitle(), movie.getPublishedDate().getYear());

        if (movies.isEmpty()) {
            return dao.insert(movie);
        } else {
            Movie duplicate = movies.get(0);
            throw new DuplicateException(String.format("Movie with title %s released on " + duplicate.getPublishedDate().toString()
                    + " already exists with id %s", duplicate.getTitle(), duplicate.getId()));
        }
    }

    @Override
    public void save(String id, Movie movie) {
        if (Objects.nonNull(getBy(id))) {
            dao.update(id, movie);
        } else {
            throw new ObjectNotFoundException(String.format("Movie %s does not exist and cannot be updated.", id));
        }
    }

    @Override
    public Movie getBy(String id) {
        Movie movie = dao.selectById(id);

        if (Objects.nonNull(movie)) {
            return movie;
        }

        throw new ObjectNotFoundException(String.format("Movie %s does not exist.", id));
    }

    @Override
    public List<Movie> getBy(MovieCategory category) {
        return dao.selectByCategory(category);
    }

    @Override
    public Set<Movie> getNewReleases(MovieCategory category) {
        return dao.selectNewRelease(category);
    }

    @Override
    public Set<Movie> getBestMovies(MovieCategory category) {
        return dao.selectBestMoviesByCategory(category);
    }

    @Override
    public List<Movie> getBy(String title, Integer year) {
        return dao.selectByTitle(title, year);
    }

    @Override
    public List<Movie> getAll() {
        return dao.selectAll();
    }

    @Override
    public void delete(String id) {
        dao.delete(id);
    }
}
