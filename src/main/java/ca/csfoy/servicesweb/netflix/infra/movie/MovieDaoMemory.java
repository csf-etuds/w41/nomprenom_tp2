package ca.csfoy.servicesweb.netflix.infra.movie;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.springframework.stereotype.Component;

import ca.csfoy.servicesweb.netflix.domaine.movie.Movie;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;
import ca.csfoy.servicesweb.netflix.infra.actor.ActorDaoMemory;

@Component
public class MovieDaoMemory implements MovieDao {

    public static final Movie MOVIE1 = new Movie("1",
            "Avengers Infinity Wars",
            "Jon Favreau",
            LocalDate.of(2018, 4, 23),
            LocalDate.of(2019, 4, 21),
            "The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe.",
            MovieCategory.ACTION,
            Map.of("Tony Stark", ActorDaoMemory.ACTOR1, "Steve Rogers", ActorDaoMemory.ACTOR2, "Bruce Banner", ActorDaoMemory.ACTOR3));
    public static final Movie MOVIE2 = new Movie("2",
            "Avengers End Games",
            "Anthony Russo",
            LocalDate.of(2019, 4, 22),
            LocalDate.of(2020, 4, 22),
            "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.",
            MovieCategory.ACTION,
            Map.of("Tony Stark", ActorDaoMemory.ACTOR1, "Steve Rogers", ActorDaoMemory.ACTOR2, "Bruce Banner", ActorDaoMemory.ACTOR3));
    public static final Movie MOVIE3 = new Movie("2",
            "Avengers Age of Ultron",
            "Joss Whedon",
            LocalDate.of(2019, 4, 22),
            LocalDate.of(2020, 4, 22),
            "When Tony Stark and Bruce Banner try to jump-start a dormant peacekeeping program called Ultron, things go horribly wrong and it's up to Earth's mightiest heroes to stop the villainous Ultron from enacting his terrible plan.",
            MovieCategory.ACTION,
            Map.of("Tony Stark", ActorDaoMemory.ACTOR1, "Steve Rogers", ActorDaoMemory.ACTOR2, "Bruce Banner", ActorDaoMemory.ACTOR3));    
    
    private Map<String, Movie> movies;

    public MovieDaoMemory() {
        this.movies = new HashMap<>();
        insert(MOVIE1);
        insert(MOVIE2);
        insert(MOVIE3);
    }

    @Override
    public boolean doesExist(String id) {
        return movies.containsKey(id);
    }

    @Override
    public Movie selectById(String id) {
        return movies.get(id);
    }

    @Override
    public List<Movie> selectAll() {
        return movies.values().stream().toList();
    }

    @Override
    public Movie insert(Movie movie) {
        movies.put(movie.getId(), movie);
        return movie;
    }

    @Override
    public void update(String id, Movie movie) {
        movies.put(id, movie);
    }

    @Override
    public void delete(String id) {
        movies.remove(id);
    }

    @Override
    public List<Movie> selectByTitle(String title, Integer year) {
        Set<Movie> searchMovies = new HashSet<>();

        if (Objects.nonNull(title) && Objects.nonNull(year)) {
            
            searchMovies.addAll(selectAll().stream().filter(m -> m.getTitle().equals(title) && m.getPublishedDate().getYear() == year).toList());
        }

        if (Objects.nonNull(title)) {
            searchMovies.addAll(selectAll().stream().filter(m -> m.getTitle().equals(title)).toList());
        }

        if (Objects.nonNull(year)) {
            searchMovies.addAll(selectAll().stream().filter(m -> m.getPublishedDate().getYear() == year).toList());
        }

        return searchMovies.stream().toList();
    }

    @Override
    public List<Movie> selectByCategory(MovieCategory category) {
        Set<Movie> searchMovies = new HashSet<>();

        if (Objects.nonNull(category)) {
            searchMovies.addAll(selectAll().stream().filter(m -> m.getCategory() == category).toList());
        }

        return searchMovies.stream().toList();
    }

    @Override
    public Set<Movie> selectNewRelease(MovieCategory category) {
        Set<Movie> searchMovies = new HashSet<>();

        if (Objects.nonNull(category)) {
            searchMovies.addAll(selectAll().stream()
                    .filter(m -> m.getAcquisitionDate().isAfter(LocalDate.now().minusDays(20)))
                    .filter(m -> m.getCategory() == category)
                    .toList());
        }

        return searchMovies;
    }

    @Override
    public Set<Movie> selectBestMoviesByCategory(MovieCategory category) {
        return Set.of();
    }
}
