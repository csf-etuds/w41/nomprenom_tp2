package ca.csfoy.servicesweb.netflix.infra.actor;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.springframework.stereotype.Component;

import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;

@Component
public class ActorDaoMemory implements ActorDao {

    public static final Actor ACTOR1 = new Actor("1", "Robert", "Downey Jr", LocalDate.of(1965, 4, 4), "Manhanttan, New York");
    public static final Actor ACTOR2 = new Actor("2", "Chris", "Evans", LocalDate.of(1981, 6, 13), "Boston, MA");
    public static final Actor ACTOR3 = new Actor("3", "Mark", "Ruffalo", LocalDate.of(1967, 11, 22), "Kenosha, WI");
    public static final Actor ACTOR4 = new Actor("4", "Paul", "Banks", LocalDate.of(1984, 11, 22), "Manhanttan, New York");
    public static final Actor ACTOR5 = new Actor("5", "Nathalie", "Portman", LocalDate.of(1981, 6, 9), "Jerusalem");
    public static final Actor ACTOR6 = new Actor("6", "Brie", "Larson", LocalDate.of(1989, 10, 1), "Sacrameto, CA");
    public static final Actor ACTOR7 = new Actor("7", "Evagneline", "Lilly", LocalDate.of(1979, 8, 3), "Fort Saskatchewan, Alberta");
    
    private Map<String, Actor> actors;

    public ActorDaoMemory() {
        this.actors = new HashMap<>();
        insert(ACTOR1);
        insert(ACTOR2);
        insert(ACTOR3);
        insert(ACTOR4);
        insert(ACTOR5);
        insert(ACTOR6);
        insert(ACTOR7);
    }

    @Override
    public boolean doesExist(String id) {
        return actors.containsKey(id);
    }

    @Override
    public Actor selectById(String id) {
        return actors.get(id);
    }

    @Override
    public List<Actor> selectAll() {
        return actors.values().stream().toList();
    }

    @Override
    public Actor insert(Actor trail) {
        actors.put(trail.getId(), trail);
        return trail;
    }

    @Override
    public void update(String id, Actor trail) {
        actors.put(id, trail);
    }

    @Override
    public void delete(String id) {
        actors.remove(id);
    }

    @Override
    public List<Actor> findByFirstnameAndLastname(String firstname, String lastname) {
        Set<Actor> searchResults = new HashSet<>();
        
        if (Objects.nonNull(firstname)) {
            searchResults.addAll(selectAll().stream().filter(m -> m.getFirstname().contains(firstname)).toList());
        }
        
        if (Objects.nonNull(lastname)) {
            searchResults.addAll(selectAll().stream().filter(m -> m.getLastname().contains(lastname)).toList());
        }
        
        return searchResults.stream().toList();
    }
}
