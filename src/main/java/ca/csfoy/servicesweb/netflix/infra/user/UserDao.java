package ca.csfoy.servicesweb.netflix.infra.user;

import ca.csfoy.servicesweb.netflix.domaine.user.NetflixUser;
import ca.csfoy.servicesweb.netflix.infra.Dao;

//{TP2-US4}

public interface UserDao extends Dao<String, NetflixUser> {

    NetflixUser findByEmail(String email);
    NetflixUser save(NetflixUser user);
}