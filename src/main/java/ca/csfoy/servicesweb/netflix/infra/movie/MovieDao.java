package ca.csfoy.servicesweb.netflix.infra.movie;

import java.util.List;
import java.util.Set;

import ca.csfoy.servicesweb.netflix.domaine.movie.Movie;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieCategory;
import ca.csfoy.servicesweb.netflix.infra.Dao;

public interface MovieDao extends Dao<String, Movie> {

    List<Movie> selectByCategory(MovieCategory category);

    Set<Movie> selectNewRelease(MovieCategory category);

    Set<Movie> selectBestMoviesByCategory(MovieCategory category);

    List<Movie> selectByTitle(String title, Integer year);
}
