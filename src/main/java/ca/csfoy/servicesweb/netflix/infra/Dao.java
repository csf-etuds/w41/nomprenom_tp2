package ca.csfoy.servicesweb.netflix.infra;

import java.util.List;

public interface Dao<I, E> {

    boolean doesExist(I id);

    E selectById(I id);

    List<E> selectAll();

    E insert(E element);

    void update(I id, E element);

    void delete(I id);
}
