package ca.csfoy.servicesweb.netflix.infra.user;

import java.util.Objects;

import org.springframework.stereotype.Repository;

import ca.csfoy.servicesweb.netflix.controller.exception.DuplicateException;
import ca.csfoy.servicesweb.netflix.controller.exception.ObjectNotFoundException;
import ca.csfoy.servicesweb.netflix.domaine.user.NetflixUser;
import ca.csfoy.servicesweb.netflix.domaine.user.NetflixUserDetails;
import ca.csfoy.servicesweb.netflix.domaine.user.UserRepository;

//{TP2-US4}

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final UserDao dao;

    public UserRepositoryImpl(UserDao dao) {
        this.dao = dao;
    }

    @Override
    public NetflixUser create(NetflixUser user) {
        if (Objects.isNull(dao.findByEmail(user.getEmailAddress()))) {
            return dao.save(user);
        } else {
            throw new DuplicateException("User (email: " + user.getEmailAddress() + ") already exists.");
        }
    }

    @Override
    /**
     * CustomUser from controller only contains 3 fields (made from UserDto).
     * Instead of replacing full object, we need to add the last 3 fields from existing version
     * TODO: Refactor to create two separate domain entities for User.
     */
    public void save(String id, NetflixUser user) {
        NetflixUser existingUser = dao.selectById(id);
        if (Objects.isNull(user)) {
            throw new ObjectNotFoundException("User (id:" + id + ") does not exist.");
        }
        NetflixUserDetails detailsToKeep = new NetflixUserDetails(existingUser.getEmailAddress(),
                                                                existingUser.getPassword(),
                                                                existingUser.getRole());
        NetflixUser userToSave = new NetflixUser(user.getId(), user.getFirstname(), user.getLastname(),
                                               detailsToKeep);
        dao.save(userToSave);
    }

    @Override
    public NetflixUser getBy(String id) {
        NetflixUser user = dao.selectById(id);
        if (Objects.isNull(user)) {
            throw new ObjectNotFoundException("User (id:" + id + ") does not exist.");
        }
        return user;
        
    }

    @Override
    public NetflixUser getByEmail(String email) {
        NetflixUser user = dao.findByEmail(email);
        if (Objects.isNull(user)) {
            throw new ObjectNotFoundException("User (email:" + email + ") does not exist.");
        }
        return user;
    }
}
