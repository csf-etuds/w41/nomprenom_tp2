package ca.csfoy.servicesweb.netflix.infra.actor;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Repository;

import ca.csfoy.servicesweb.netflix.controller.exception.DuplicateException;
import ca.csfoy.servicesweb.netflix.controller.exception.ObjectNotFoundException;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;
import ca.csfoy.servicesweb.netflix.domaine.actor.ActorRepository;

@Repository
public class ActorRepositoryMemory implements ActorRepository {

    private final ActorDao dao;

    public ActorRepositoryMemory(ActorDao dao) {
        this.dao = dao;
    }

    @Override
    public Actor create(Actor actor) {
        List<Actor> actors = getBy(actor.getFirstname(), actor.getLastname());
        if (actors.isEmpty()) {
            return dao.insert(actor);
        } else {
            throw new DuplicateException(String.format("Actor with firstname '%s' and lastname '%s' already exists with id '%s'",
                    actor.getFirstname(),
                    actor.getLastname(),
                    actor.getId()));
        }
    }

    @Override
    public void save(String id, Actor actor) {
        if (Objects.nonNull(getBy(id))) {
            dao.update(id, actor);
        } else {
            throw new ObjectNotFoundException(String.format("Actor %s does not exist", id));
        }        
    }

    @Override
    public void saveAll(Collection<Actor> actors) {
        actors.stream().forEach(a -> save(a.getId(), a));
    }

    @Override
    public Actor getBy(String id) {
        Actor actor = dao.selectById(id);

        if (Objects.nonNull(actor)) {
            return actor;
        }

        throw new ObjectNotFoundException(String.format("Actor with %s not found", id));
    }

    @Override
    public List<Actor> getBy(String firstname, String lastname) {
        return dao.findByFirstnameAndLastname(firstname, lastname);
    }

    @Override
    public List<Actor> getAll() {
        return dao.selectAll();
    }

    @Override
    public void delete(String id) {
        dao.delete(id);
    }
}
