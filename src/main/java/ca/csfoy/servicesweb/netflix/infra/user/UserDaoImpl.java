package ca.csfoy.servicesweb.netflix.infra.user;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import ca.csfoy.servicesweb.netflix.domaine.user.NetflixUser;
import ca.csfoy.servicesweb.netflix.domaine.user.NetflixUserDetails;
import ca.csfoy.servicesweb.netflix.domaine.user.Role;
import ca.csfoy.servicesweb.netflix.domaine.user.RoleName;

//{TP2-US4}

@Component
public class UserDaoImpl implements UserDao {

    private Map<String, NetflixUser> users;
    
    public UserDaoImpl() {
        //TODO create two admin users
        this.users = new HashMap<>();
        Role user = new Role("1", RoleName.USER);
        Role admin = new Role("2", RoleName.ADMIN);
        // Remember to use 10 rounds only for bcrypt (not 12)
        // bcrypt : this_is_my_super_password1234
        NetflixUserDetails user1 = new NetflixUserDetails("jean.tremblay@testcsfoy.ca", "$2a$10$i5fi0FMaorhWU3myzYPBgO4ebBn4WFGUGflVBS6StI.DmBFcPyn/K", user);
        // bcrypt : hashing_is_wonderful_!
        NetflixUserDetails user2 = new NetflixUserDetails("julie.tremblay@testcsfoy.ca", "$2a$10$r0eEqHZOnsbDuHTpjA5iLOdr/CgMjHl2DUCrehNxQWMu7ifUKT/v2", user);
        insert(new NetflixUser("1", "Jean", "Tremblay", user1));
        insert(new NetflixUser("2", "Julie", "Tremblay", user2));
    }

    @Override
    public boolean doesExist(String id) {
        return users.containsKey(id);
    }

    @Override
    public NetflixUser selectById(String id) {
        return users.get(id);
    }

    public NetflixUser save(NetflixUser user) {
        users.put(user.getId(), user);
        return user;
    }

    @Override
    public List<NetflixUser> selectAll() {
        return users.values().stream().toList();
    }

    @Override
    public NetflixUser insert(NetflixUser element) {
        users.put(element.getId(), element);
        return element;
    }

    @Override
    public void update(String id, NetflixUser element) {
        users.put(id, element);
    }

    @Override
    public void delete(String id) {
        users.remove(id);
    }

    @Override
    public NetflixUser findByEmail(String email) {
        //TODO : Vous devez implémenter cette recherche
        return null;
    }
}
