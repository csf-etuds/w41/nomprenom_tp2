package ca.csfoy.servicesweb.netflix.controller;

import org.springframework.web.bind.annotation.RestController;

import ca.csfoy.servicesweb.netflix.api.HealthResource;

@RestController
public class HealthController implements HealthResource {

    @Override
    public String getHealth() {
        return "Up and running";
    }
}
