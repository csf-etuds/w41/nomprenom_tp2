package ca.csfoy.servicesweb.netflix.controller.validations;

import org.springframework.context.ApplicationContext;

public class ValidatorFactory {
    
    private final ApplicationContext applicationContext;
    
    public ValidatorFactory(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
