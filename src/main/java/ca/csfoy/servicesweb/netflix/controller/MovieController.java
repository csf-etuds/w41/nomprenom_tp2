package ca.csfoy.servicesweb.netflix.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RestController;

import ca.csfoy.servicesweb.netflix.api.MovieResource;
import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDtoWithoutId;
import ca.csfoy.servicesweb.netflix.api.dto.LightMovieDto;
import ca.csfoy.servicesweb.netflix.domaine.movie.Movie;
import ca.csfoy.servicesweb.netflix.domaine.movie.MovieRepository;

@RestController
public class MovieController implements MovieResource {

    private final MovieRepository repository;
    private final MovieConverter converter;

    public MovieController(MovieRepository repository, MovieConverter converter) {
        this.repository = repository;
        this.converter = converter;
    }

    public FullMovieDto create(FullMovieDtoWithoutId movie) {
        Movie movieCreated = repository.create(converter.toMovieFromFullDtoWithoutId(movie));
        return converter.toFullDtoFromMovie(movieCreated);
    }

    public FullMovieDto getById(String id) {
        return converter.toFullDtoFromMovie(repository.getBy(id));
    }

    public List<LightMovieDto> getAll() {
        return converter.fromMovie(repository.getAll());
    }

    public List<LightMovieDto> search(String title, Integer year) {
        return converter.fromMovie(repository.getBy(title, year));
    }

    public void update(String id, FullMovieDto movie) {
        repository.save(id, converter.toMovieFromFullDto(movie));
    }

    public void delete(String id) {
        repository.delete(id);
    }

}
