package ca.csfoy.servicesweb.netflix.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RestController;

import ca.csfoy.servicesweb.netflix.api.ActorResource;
import ca.csfoy.servicesweb.netflix.api.dto.FullActorDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullActorDtoWithoutId;
import ca.csfoy.servicesweb.netflix.api.dto.LightActorDto;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;
import ca.csfoy.servicesweb.netflix.domaine.actor.ActorRepository;

@RestController
public class ActorController implements ActorResource {

    private final ActorRepository repository;
    private final ActorConverter converter;

    public ActorController(ActorRepository repository, ActorConverter converter) {
        this.repository = repository;
        this.converter = converter;
    }

    public FullActorDto createActor(FullActorDtoWithoutId actor) {
        Actor actorCreated = repository.create(converter.toActorFromFullDtoWithoutId(actor));
        return converter.fromActor(actorCreated);
    }

    public FullActorDto getActor(String id) {
        return converter.fromActor(repository.getBy(id));
    }

    public List<LightActorDto> getActors() {
        return converter.fromActorListToLightDtoList(repository.getAll());
    }

    public void updateActor(String id, FullActorDto actor) {
        repository.save(id, converter.toActor(actor));
    }

    public void deleteActor(String id) {
        repository.delete(id);
    }
}
