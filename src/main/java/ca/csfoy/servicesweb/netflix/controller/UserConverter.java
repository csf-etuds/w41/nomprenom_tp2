package ca.csfoy.servicesweb.netflix.controller;

import java.util.Objects;
import java.util.UUID;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import ca.csfoy.servicesweb.netflix.api.dto.FullUserDto;
import ca.csfoy.servicesweb.netflix.api.dto.UserDto;
import ca.csfoy.servicesweb.netflix.domaine.user.NetflixUser;
import ca.csfoy.servicesweb.netflix.domaine.user.NetflixUserDetails;
import ca.csfoy.servicesweb.netflix.domaine.user.Role;
import ca.csfoy.servicesweb.netflix.domaine.user.RoleName;

@Component
public class UserConverter {

    private final PasswordEncoder passwordEncoder;

    public UserConverter(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto fromUser(NetflixUser user) {
        return new UserDto(user.getId(),
                user.getFirstname(),
                user.getLastname());
    }

    public FullUserDto fromUserToFullUserDto(NetflixUser user) {
        return new FullUserDto(user.getId(),
                user.getFirstname(),
                user.getLastname(),
                user.getEmailAddress(),
                null);
    }

    public NetflixUser toUser(UserDto user) {
        if (Objects.isNull(user.id)) {
            return new NetflixUser(UUID.randomUUID().toString(),
                    user.firstname,
                    user.lastname,
                    new NetflixUserDetails(null, null, null));
        }

        return new NetflixUser(user.id,
                user.firstname,
                user.lastname,
                new NetflixUserDetails(null, null, null));
    }

    public NetflixUser toUser(FullUserDto user) {
        if (Objects.isNull(user.id)) {
            return new NetflixUser(UUID.randomUUID().toString(),
                    user.firstname,
                    user.lastname,
                    new NetflixUserDetails(user.emailAddress, passwordEncoder.encode(user.password)));
        }

        return new NetflixUser(user.id,
                user.firstname,
                user.lastname,
                new NetflixUserDetails(user.emailAddress, passwordEncoder.encode(user.password)));
    }
}
