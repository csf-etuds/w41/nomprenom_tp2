package ca.csfoy.servicesweb.netflix.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullMovieDtoWithoutId;
import ca.csfoy.servicesweb.netflix.api.dto.LightMovieDto;
import ca.csfoy.servicesweb.netflix.domaine.movie.Movie;

@Component
public class MovieConverter {
    
    private final ActorConverter actorConverter;
    
    public MovieConverter(ActorConverter actorConverter) {
        this.actorConverter = actorConverter;
    }

    public Movie toMovieFromFullDtoWithoutId(FullMovieDtoWithoutId movie) {
        return new Movie(movie.title, 
                movie.director, 
                movie.publishedDate, 
                movie.description, 
                movie.category,
                actorConverter.toActorMap(movie.casting));
    }

    public Movie toMovieFromFullDto(FullMovieDto movie) {
        return new Movie(movie.id, 
                movie.title, 
                movie.director, 
                movie.publishedDate, 
                movie.description, 
                movie.category,
                actorConverter.toActorMap(movie.casting));
    }
    
    public LightMovieDto fromMovie(Movie movie) {
        return new LightMovieDto(movie.getId(), 
                movie.getTitle(), 
                movie.getCategory());
    }
    
    public FullMovieDto toFullDtoFromMovie(Movie movie) {
        return new FullMovieDto(movie.getId(), 
                movie.getTitle(), 
                movie.getDirector(),
                movie.getPublishedDate(),
                movie.getDescription(),
                movie.getCategory(),
                actorConverter.fromActorMap(movie.getCasting()));
    }   
    
    public List<LightMovieDto> fromMovie(List<Movie> movies) {
        return movies.stream()
                .map(this::fromMovie)
                .toList();
    }
    
    public Set<LightMovieDto> fromMovie(Set<Movie> movies) {
        return movies.stream()
                .map(this::fromMovie)
                .collect(Collectors.toSet());
    }
}
