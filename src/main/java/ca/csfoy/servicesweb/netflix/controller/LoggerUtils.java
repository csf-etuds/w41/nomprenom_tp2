package ca.csfoy.servicesweb.netflix.controller;

import java.time.LocalDateTime;
import java.util.UUID;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.springframework.http.ProblemDetail;

import io.micrometer.common.util.StringUtils;

public class LoggerUtils {

    public static final String LOGGING_FORMAT = "[%s](id:%s)-> Message:%s, Trace:%s";

    public static void logError(Logger logger, Throwable ex, String errorId) {
        logger.error(String.format(LOGGING_FORMAT,
                                   LocalDateTime.now().toString(),
                                   errorId,
                                   ex.getMessage(),
                                   ExceptionUtils.getStackTrace(ex)));
    }

}
