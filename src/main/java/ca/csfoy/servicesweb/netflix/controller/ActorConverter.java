package ca.csfoy.servicesweb.netflix.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import ca.csfoy.servicesweb.netflix.api.dto.FullActorDto;
import ca.csfoy.servicesweb.netflix.api.dto.FullActorDtoWithoutId;
import ca.csfoy.servicesweb.netflix.api.dto.LightActorDto;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;

@Component
public class ActorConverter {
    
    public Actor toActorFromFullDtoWithoutId(FullActorDtoWithoutId actor) {     
        return new Actor(actor.firstname, actor.lastname, actor.birthdate, actor.birthplace);
    }

    public Actor toActor(FullActorDto actor) {      
        return new Actor(actor.id, actor.firstname, actor.lastname, actor.birthdate, actor.birthplace);
    }
    
    public FullActorDto fromActor(Actor actor) {
        return new FullActorDto(actor.getId(), actor.getFirstname(), actor.getLastname(), actor.getBirthdate(), actor.getBirthplace());
    }
    
    public List<LightActorDto> fromActorListToLightDtoList(List<Actor> actors) {
        return actors.stream().map(this::fromActorToLightDto)
                .toList();
    }
    
    private LightActorDto fromActorToLightDto(Actor actor) {
        return new LightActorDto(actor.getId(), actor.getFirstname(), actor.getLastname());
    }

    public Map<String, Actor> toActorMap(Map<String, FullActorDto> actors) {
        Map<String, Actor> actorsConverted = new HashMap<>();
        actors.keySet().stream()
                .forEach(key -> actorsConverted.put(key, toActor(actors.get(key))));
        
        return actorsConverted;
    }
    
    public Map<String, FullActorDto> fromActorMap(Map<String, Actor> actors) {
        Map<String, FullActorDto> actorsConverted = new HashMap<>();
        actors.keySet().stream()
                .forEach(key -> actorsConverted.put(key, fromActor(actors.get(key))));
        
        return actorsConverted;
    }
}
