package ca.csfoy.servicesweb.netflix.controller;

import org.springframework.web.bind.annotation.RestController;

import ca.csfoy.servicesweb.netflix.api.UserResource;
import ca.csfoy.servicesweb.netflix.api.dto.FullUserDto;
import ca.csfoy.servicesweb.netflix.api.dto.UserDto;
import ca.csfoy.servicesweb.netflix.domaine.user.UserRepository;

@RestController
public class UserController implements UserResource {

    private final UserRepository repo;
    private final UserConverter converter;

    public UserController(UserRepository repo, UserConverter converter) {
        this.repo = repo;
        this.converter = converter;
    }

    @Override
    public void createUser(UserDto user) {
        repo.create(converter.toUser(user));
    }

    @Override
    public void createUser(FullUserDto user) {
        repo.create(converter.toUser(user));
    }

    @Override
    public UserDto getUser(String id) {
        return converter.fromUser(repo.getBy(id));
    }

    @Override
    public void modifyUser(String id, UserDto user) {
        repo.save(id, converter.toUser(user));
    }

}
