package ca.csfoy.servicesweb.netflix.controller;

import java.net.URI;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.stereotype.Component;

//{TP2-US4}

@Component
public class NetflixProblemDetailFactory {
    
    public static final String DEV_API_DOCUMENTATION = "http://localhost:8080/swagger-ui/index.html";
    
    public ProblemDetail netflixProblemDetail(Throwable e, HttpStatus statusCode, String errorId, boolean expected) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(statusCode, e.getMessage());
        problemDetail.setType(URI.create(DEV_API_DOCUMENTATION));
        problemDetail.setProperty("expected", expected);
        problemDetail.setProperty("id", errorId);
        return problemDetail;
    }
}
