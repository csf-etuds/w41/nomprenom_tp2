package ca.csfoy.servicesweb.netflix.controller;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import ca.csfoy.servicesweb.netflix.controller.exception.DuplicateException;
import ca.csfoy.servicesweb.netflix.controller.exception.ObjectNotFoundException;

//{TP2-US4} - Modification pour supporter la nouvelle factory de NetflixProblemDetail.

@RestControllerAdvice
public class NetflixExceptionHandler /*extends ResponseEntityExceptionHandler*/ {

    public static final String DUPLICATE_TITLE_MSG = "This resource already exist on the server.";
    public static final String OBJECTNOTFOUND_TITLE_MSG = "An object requested was not found on the server.";
    public static final String CONSTRAINT_VIOLATION_TITLE_MSG = "An input does not comply with a validation constraint.";
    public static final String INPUT_ARG_TITLE_MSG = "The provided input does not comply to one or more validations contraints.";
    public static final String ERROR_TITLE_MSG = "An error has occured.";    
    
    private Logger logger = LoggerFactory.getLogger(NetflixProblemDetailFactory.class);
    
    @Autowired
    private NetflixProblemDetailFactory problemDetailFactory;

    @ExceptionHandler(DuplicateException.class)
    public ProblemDetail handleDuplicateException(DuplicateException ex) {
        String errorId = UUID.randomUUID().toString();
        ProblemDetail pd = problemDetailFactory.netflixProblemDetail(ex, HttpStatus.UNPROCESSABLE_ENTITY, errorId, true);
        pd.setTitle(NetflixExceptionHandler.DUPLICATE_TITLE_MSG);
        LoggerUtils.logError(logger, ex, errorId);
        return pd;

    }

    @ExceptionHandler(ObjectNotFoundException.class)
    public ProblemDetail handleObjectNotFoundException(ObjectNotFoundException ex) {
        String errorId = UUID.randomUUID().toString();
        ProblemDetail pd = problemDetailFactory.netflixProblemDetail(ex, HttpStatus.NOT_FOUND, errorId, true);
        pd.setTitle(NetflixExceptionHandler.OBJECTNOTFOUND_TITLE_MSG);
        LoggerUtils.logError(logger, ex, errorId);
        return pd;
    }

    @ExceptionHandler(Exception.class)
    public ProblemDetail handleException(Exception ex) {
        String errorId = UUID.randomUUID().toString();
        ProblemDetail pd = problemDetailFactory.netflixProblemDetail(ex, HttpStatus.UNPROCESSABLE_ENTITY, errorId, false);
        pd.setTitle(NetflixExceptionHandler.ERROR_TITLE_MSG);
        LoggerUtils.logError(logger, ex, errorId);
        return pd;
    }
  
}
