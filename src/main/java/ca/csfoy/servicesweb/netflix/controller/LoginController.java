package ca.csfoy.servicesweb.netflix.controller;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RestController;

import ca.csfoy.servicesweb.netflix.api.LoginResource;
import ca.csfoy.servicesweb.netflix.api.dto.TokenDto;
import ca.csfoy.servicesweb.netflix.api.dto.UserCredentialsDto;
import ca.csfoy.servicesweb.netflix.domaine.user.NetflixUser;
import ca.csfoy.servicesweb.netflix.domaine.user.UserRepository;
import ca.csfoy.servicesweb.netflix.security.JwtTokenManager;

@RestController
public class LoginController implements LoginResource {

    private final UserRepository repo;
    private final AuthenticationManager authManager;
    private final JwtTokenManager tokenManager;

    public LoginController(UserRepository repo, AuthenticationManager authManager, JwtTokenManager tokenManager) {
        this.repo = repo;
        this.authManager = authManager;
        this.tokenManager = tokenManager;
    }

    @Override
    public TokenDto loginUser(UserCredentialsDto user) {
        authManager.authenticate(new UsernamePasswordAuthenticationToken(user.emailAddress, user.password));
        //TODO get a NetflixUser and create a token that could be returned.
        return null;
    }
}
